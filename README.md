## White Christmas (Client mod)
After changing seasons were implemented into Wurm there was only a small chance of white christmas anymore. Previously in the early days of Wurm it was only but always winter during christmas. White christmas in Wurm was a common thing and all players were looking forward to this time and were hyped for Santa to arrive at the spawn towns.

Therefore, we want to bring this time back to Wurm Unlimited and introduce the White Christmas client mod to you. This mod will override the current season clientside with winter around christmas. We defined christmas time from 23rd of December to 1st of January (including start and end date) every year. Similar to how it was before changing seasons were implemented into Wurm.

This mod requires Ago's client modloader that can be downloaded from GitHub: https://github.com/ago1024/WurmClientModLauncher/releases
To install this mod simply put all files into your mod folder of your Wurm Unlimited Client, similar to every other Wurm Unlimited mod installation. This mod will have priority over bdew's no winter mod, therefore bdew's no winter mod will stop working after applying this mod.
**You are using this mod at your own risk!**

Please report any found problems as a GitLab issue, I will look into them. In case you find any problem please include as many details as possible and screenshots of the problem.

## Settings
By default this mod will overwrite the season from 23rd of December to 1st of January (including start and end date) every year to Winter. Outside of this time range the normal season of the server will be used by default.

However, there are multiple settings that can be used to lock the season outside of the christmas time range. The following settings can be adjusted in the properties file:

    #0...Mod disabled
    #1...White Christmas then normal seasons (default)
    #2...White Christmas otherwise locked season (eg. no winter). -> Lock season with seasonLockValue (default summer)
    seasonModificationValue=1
    
    #This parameter requires the seasonModificationValue to have the value 2, otherwise it will have no effect. (Locks season outside of christmas time range)
    #0...Winter
    #1...Spring
    #2...Summer (default)
    #3...Fall
    seasonLockValue=2
