package com.sklotopolis.mods.client.whitechristmas;

import com.wurmonline.client.game.SeasonManager;
import org.gotti.wurmunlimited.modloader.classhooks.HookManager;
import org.gotti.wurmunlimited.modloader.interfaces.Configurable;
import org.gotti.wurmunlimited.modloader.interfaces.Initable;
import org.gotti.wurmunlimited.modloader.interfaces.PreInitable;
import org.gotti.wurmunlimited.modloader.interfaces.WurmClientMod;

import java.util.Calendar;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WhiteChristmasMod implements WurmClientMod, Initable, PreInitable, Configurable {
    private static final Logger logger = Logger.getLogger("WhiteChristmasMod");

    /*
        0...Mod disabled
        1...White Christmas then normal seasons (default)
        2...White Christmas but locked season (eg. no winter). -> Lock season with seasonLockValue (default summer)
     */
    private static int seasonModificationValue = 1;

    /*
        0...Winter
        1...Spring
        2...Summer
        3...Fall
     */
    private static int seasonLockValue = 2;

    @Override
    public void configure(Properties properties) {
        seasonModificationValue = Integer.parseInt(properties.getProperty("seasonModificationValue", "1"));
        seasonLockValue = Integer.parseInt(properties.getProperty("seasonLockValue", "2"));
        if (seasonLockValue < 0 || seasonLockValue > 3) {
            seasonLockValue = 2;
            logger.warning("Illegal season locak value in WhiteChristmasMod. Setting to default 2 (summer).");
        }
        if (seasonModificationValue < 0 || seasonModificationValue > 2) {
            seasonModificationValue = 0;
            logger.warning("Illegal season modification value in WhiteChristmasMod. Setting to default 0 (mod disabled).");
        }
    }

    @Override
    public void init() {
        try {
            HookManager.getInstance().registerHook("com.wurmonline.client.game.SeasonManager",
                    "selectSeason", "(FF)Lcom/wurmonline/client/game/SeasonManager$Season;",
                    () -> (proxy, method, args) -> {
                        if (seasonModificationValue >= 1 && isChristmas()) {
                            //White Christmas
                            return SeasonManager.Season.values()[0];
                        } else if (seasonModificationValue == 2) {
                            //White Christmas and locked season for the rest of the year
                            return SeasonManager.Season.values()[seasonLockValue];
                        } else {
                            return method.invoke(proxy, args);
                        }
                    });
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error injecting WhiteChristmasMod", e);
        }

    }

    private static boolean isChristmas() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        return (calendar.get(Calendar.MONTH) == Calendar.DECEMBER
                && calendar.get(Calendar.DAY_OF_MONTH) >= 23) ||
                (calendar.get(Calendar.MONTH) == Calendar.JANUARY && calendar.get(Calendar.DAY_OF_MONTH) <= 1);
    }

    @Override
    public void preInit() {

    }
}